"use strict"
function createNewUser() {
    let firstName = prompt("Введіть своє ім'я:");
    let lastName = prompt("Введіть своє прізвище:");
    let birthdayStr = prompt("Введіть свою дату народження у форматі dd.mm.yyyy:");
    let birthdayArr = birthdayStr.split(".");
    let birthday = new Date(birthdayArr[2], birthdayArr[1] - 1, birthdayArr[0]);
    
    function getPassword() {
      return firstName.charAt(0).toUpperCase() + lastName.toLowerCase() + birthday.getFullYear();
    }
    
    function getAge() {
      let ageDifference = Date.now() - birthday.getTime();
      let ageDate = new Date(ageDifference);
      return Math.abs(ageDate.getUTCFullYear() - 1970);
    }
  
    let newUser = {
      getLogin() {
        return firstName.charAt(0).toLowerCase() + lastName.toLowerCase();
      },
      getPassword,
      getAge
    };
  
    Object.defineProperties(newUser, {
      firstName: {
        get() {
          return firstName;
        }
      },
      lastName: {
        get() {
          return lastName;
        }
      },
      birthday: {
        get() {
          return birthday;
        }
      },
      setFirstName: {
        value: function(newName) {
          firstName = newName;
        }
      },
      setLastName: {
        value: function(newName) {
          lastName = newName;
        }
      }
    });
  
    return newUser;
  }
  
  let user = createNewUser();
  console.log(user.getLogin());
  console.log("Age: ", user.getAge());
  console.log("Password: ", user.getPassword());
